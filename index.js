//<---------------headers start----------------->
const express = require('express');
const http = require('http');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const dishRouter = require('./routes/dishRouter');
const leadersRouter = require('./routes/leadersRouter');
const promoRouter = require('./routes/promoRouter');
//<---------------headers end----------------->

const hostName = 'localhost';    //setting hostName 
const port = 3000;               //setting port

const app = express();

//<---------------middleware start----------------->
app.use(morgan('dev'));
app.use(express.static(__dirname + '/public'));
app.use(bodyParser.json());

//<---------------router start----------------->
app.use('/dishes', dishRouter);
app.use('/leaders', leadersRouter);
app.use('/promotions', promoRouter);
//<---------------routers end----------------->

//<---------------middleware end----------------->

//<---------------server starting point----------------->
app.use((req, res, next) => {     //This is where request are handled
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/html');
    res.end('<html><body><h1>Hello World</h1></body></html>');
});
const server = http.createServer(app);
server.listen(port, hostName, ()=>{
    console.log(`Server running at http://${hostName}:${port}`);
});
//<---------------Server endpoint----------------->