const express = require('express');
const bodyParser = require('body-parser');

const dishRouter = express.Router();
dishRouter.use(bodyParser.json());

dishRouter.route('/:dishId')
.all((req, res, next)=>{
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    next();
})
.get((req, res)=>{
    res.end('Will send you the details of the dish: ' + req.params.dishId);
})
.post((req, res)=>{
    res.statusCode = 403;
    res.end('POST operation not upported on /disher/' + req.params.dishId);
})
.put((req, res)=>{
    res.write('Updating the dish: ' + req.params.dishId + '\n');
    res.end('Updated');
})
.delete((req, res)=>{
    res.end('Deleting dish: ' + req.params.dishId);
});

dishRouter.route('/')
.all((req, res, next)=>{
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    next();
})
.get((req, res)=>{
    if(req.params.dishId != undefined){
        res.write(req.params.dishId+"");
    }
    res.end('Will send you all the dishes!');
})
.post((req, res)=>{
    res.end('will add dish: ' + req.body.name + " with details: " + req.body.description);
})
.put( (req, res)=>{
    res.statusCode = 403;
    res.end('put operation not supported');
})
.delete((req, res)=>{
    res.end('Deleting all the dishes!');
});


module.exports = dishRouter;