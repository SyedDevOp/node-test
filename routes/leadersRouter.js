const express = require('express');
const bodyParser = require('body-parser');

const leadersRouter = express.Router();
leadersRouter.use(bodyParser.json());

leadersRouter.route('/')
.all((req, res, next)=>{
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    next();
})
.get((req, res)=>{
    res.end('Will send you all the leaders!')
})
.post((req, res)=>{
    res.end('Will add the leader name: ' + req.body.name + ' and discrpition: ' + req.body.desc);
})
.put((req, res)=>{
    res.statusCode = 403;
    res.end('put operation not Supported');
})
.delete((req, res)=>{
    res.end('Deleting all the Leaders!');
});

leadersRouter.route('/:leaderId')
.all((req, res, next)=>{
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    next();
})
.get((req, res)=>{
    res.end('Will send you the information of the leader# ' + req.params.leaderId);
})
.post((req, res)=>{
    res.statusCode = 403;
    res.end('POST operation not upported on /leaders/' + req.params.leaderId);
})
.put((req, res)=>{
    res.write('Updating the Leader ' + req.params.leaderId);
    res.end('Updated');
})
.delete((req, res)=>{
    res.end('Deleting the Leader: ' + req.params.leaderId);
});
module.exports = leadersRouter;