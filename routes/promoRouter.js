const express = require('express');
const bodyParser = require('body-parser');

const promoRouter = express.Router();
promoRouter.use(bodyParser.json());

promoRouter.route('/')
.all((req, res, next)=>{
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    next();
})
.get((req, res)=>{
    res.end('Will send you all the Promotions!')
})
.post((req, res)=>{
    res.end('Will add the Promotion name: ' + req.body.name + ' with discrpition: ' + req.body.desc);
})
.put((req, res)=>{
    res.statusCode = 403;
    res.end('put operation not Supported');
})
.delete((req, res)=>{
    res.end('Deleting all the Promotions!');
});
promoRouter.route('/:promoId')
.all((req, res, next)=>{
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    next();
})
.get((req, res)=>{
    res.end('Will send you the information of the Promontion# ' + req.params.promoId);
})
.post((req, res)=>{
    res.statusCode = 403;
    res.end('POST operation not upported on /promotions/' + req.params.promoId);
})
.put((req, res)=>{
    res.write('Updating the promotion ' + req.params.promoId);
    res.end('Updated');
})
.delete((req, res)=>{
    res.end('Deleting the promotion: ' + req.params.promoId);
});
module.exports = promoRouter;